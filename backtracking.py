board = [[3, 0, 6, 5, 0, 8, 4, 0, 0],
         [5, 2, 0, 0, 0, 0, 0, 0, 0],
         [0, 8, 7, 0, 0, 0, 0, 3, 1],
         [0, 0, 3, 0, 1, 0, 0, 8, 0],
         [9, 0, 0, 8, 6, 3, 0, 0, 5],
         [0, 5, 0, 0, 9, 0, 6, 0, 0],
         [1, 3, 0, 0, 0, 0, 2, 5, 0],
         [0, 0, 0, 0, 0, 0, 0, 7, 4],
         [0, 0, 5, 2, 0, 6, 3, 0, 0]]


def print_board(bo):
    for i in range(len(bo)):
        if i % 3 == 0 and i != 0:
            print('- - - - - - - - - - - - - - - -')
        for j in range(len(bo[0])):
            if j % 3 == 0 and j != 0:
                print(' | ', end='')
            print(bo[i][j], end='  ')
        print()


def find_entry(bo):
    for i in range(len(bo)):
        for j in range(len(bo[0])):
            if bo[i][j] == 0:
                return i, j
    return False


def valid(bo, row, col, num):
    for i in range(9):
        if bo[row][i] == num:
            return False

    for i in range(9):
        if bo[i][col] == num:
            return False

    start_row = row - row % 3
    start_col = col - col % 3

    for i in range(start_row, start_row + 3):
        for j in range(start_col, start_col + 3):
            if bo[i][j] == num:
                return False
    return True


def solve(bo):
    pos = find_entry(bo)

    if not isinstance(pos, tuple):
        return True

    row, col = pos

    for num in range(1, 10):
        if valid(bo, row, col, num):
            bo[row][col] = num

            if solve(bo):
                return True

        bo[row][col] = 0
    return False


if __name__ == '__main__':
    if solve(board):
        print_board(board)
    else:
        print('no solutions exists')